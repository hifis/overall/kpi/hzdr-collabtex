# Service Usage

* HZDR Collabtex

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data

| Data                                         | Unit            | Weighting | Comment              |
|----------------------------------------------|-----------------|-----------|----------------------|
| active_users_csv.number_of_users             | Quantity/Number | 0.25      |                      |
| active_users_csv.last_active_1_days          | Quantity/Number | 0         |                      |
| active_users_csv.last_active_7_days          | Quantity/Number | 0.25      |                      |
| active_users_csv.last_active_14_days         | Quantity/Number | 0         |                      |
| active_users_csv.last_active_30_days         | Quantity/Number | 0         |                      |
| active_users_csv.last_active_60_days         | Quantity/Number | 0         |                      |
| overleaf_email_domains_csv                   | Quantity/Number | 0         | Origin by domain     |
| overleaf_user_origins_csv                    | Quantity/Number | 0         | Origin by VO         |
| project_statistics_csv.number_of_projects    | Quantity/Number | 0.25      |                      |
| project_statistics_csv.last_active_1_days    | Quantity/Number | 0         |                      |
| project_statistics_csv.last_active_7_days    | Quantity/Number | 0.25      |                      |
| project_statistics_csv.last_active_14_days   | Quantity/Number | 0         |                      |
| project_statistics_csv.last_active_30_days   | Quantity/Number | 0         |                      |
| project_statistics_csv.last_active_60_days   | Quantity/Number | 0         |                      |


## Schedule

* daily


